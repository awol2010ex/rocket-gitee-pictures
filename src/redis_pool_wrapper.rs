use deadpool_redis::{Pool, Config};

//获取redis连接池
pub fn get_pool(password :&str ,host :&str ,port :&i32) -> Pool {
    let mut cfg = Config::default();
    cfg.url = Some(format!("redis://:{}@{}:{}",
       &password,
       &host,
       &port   
    ));
    cfg.create_pool().unwrap()
}
