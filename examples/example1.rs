#[macro_use] extern crate rocket;
//gitee images repo wrapper
extern crate rocket_gitee_pictures;
use rocket_gitee_pictures::{RocketGiteePicturesHandler,RocketGiteePicturesWrapperConfig};
use std::path::PathBuf;
use std::env;
/*

ex:

cargo run --example example1



*/

#[get("/<path..>")]
async  fn get_image_path(path: PathBuf) -> String {
    format!("path={}", path.to_str().unwrap().replace("\\","/"))
}






#[rocket::main]
async fn main()  {

    //配置
    let  config =RocketGiteePicturesWrapperConfig{
        redis_host :env::var("redis_host").unwrap(),
        redis_port :env::var("redis_port").unwrap().parse::<i32>().unwrap(),
        redis_password :env::var("redis_password").unwrap(),
        gitee_token :env::var("gitee_token").unwrap(),
        gitee_owner :env::var("gitee_owner").unwrap(),
        gitee_repo :env::var("gitee_repo").unwrap(),
        gitee_branch :env::var("gitee_branch").unwrap(),
        gitee_refresh_interval : env::var("gitee_refresh_interval").unwrap().parse::<i32>().unwrap(),
    };


    let  builder =rocket::build()
    .mount("/images/path", routes![get_image_path])
    .mount("/images/gitee", RocketGiteePicturesHandler::routes("/<path..>",config).await);
    
    
    builder.launch().await.unwrap_err();
}